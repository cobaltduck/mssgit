describe("Category Controller", function() {
		
	beforeEach(module('marineService'));
	
	var $httpBackend, $rootScope, $routeParams, createController;
	
	beforeEach(inject(function ($injector) {
		// Set up the mock http service responses
	    $httpBackend = $injector.get('$httpBackend');
	    
	    // Get hold of a scope (i.e. the root scope)
		$rootScope = $injector.get('$rootScope');
		
		// Set up route params
		$routeParams = $injector.get('$routeParams');
		
		// The $controller service is used to create instances of controllers
		var $controller = $injector.get('$controller');
		createController = function() {
		   return $controller('categoryCtrl', {'$scope' : $rootScope });
		};
	    
		
		// backend mock defintions
	    $httpBackend.when('GET', '/MarineServiceScheduler/websvc/category/list')
	    	.respond( [
	    	        {categoryId: 1, categoryName: 'Fred'},
	    	        {categoryId: 2, categoryName: 'Barney'},
	    	        {categoryId: 3, categoryName: 'Dino'},
	    	] );
	    
	    $httpBackend.when('GET', '/MarineServiceScheduler/websvc/category/retrieve/1')
	    	.respond (
	    			{ categoryId: 1, categoryName: 'Fred'}
	    	);
	    
	    $httpBackend.when('POST', '/MarineServiceScheduler/websvc/category/add')
    		.respond (true);
	    $httpBackend.when('POST', '/MarineServiceScheduler/websvc/category/update')
			.respond (true);
	    $httpBackend.when('GET', '/MarineServiceScheduler/websvc/category/delete/1')
		.respond (true);

	    
	}));
		
	it("retrieves a list of categories", function() {
		createController();
		$httpBackend.expectGET('/MarineServiceScheduler/websvc/category/list');
		$rootScope.getList(false);
		$httpBackend.flush();
		
		expect($rootScope.message).toBeDefined();
		expect($rootScope.message.text).toBeUndefined();
		expect($rootScope.categoryList).toBeDefined();
		expect($rootScope.categoryList.length).toBe(3);
	});
	
	it("loads an individual category", function() {
		createController();
		$httpBackend.expectGET('/MarineServiceScheduler/websvc/category/retrieve/1');
		$routeParams.categoryId = 1;
		$routeParams.state = 'edit'
		$rootScope.load();
		$httpBackend.flush();
		
		expect($rootScope.message).toBeDefined();
		expect($rootScope.message.text).toBeUndefined();
		expect($rootScope.categoryItem).toBeDefined();
		expect($rootScope.categoryItem.categoryId).toBe(1);
		expect($rootScope.categoryItem.categoryName).toBe('Fred');
		expect($rootScope.buttonStatus).toBeDefined();
	});
	
	it("saves a new valid category", function() {
		createController();
		$rootScope.isEdit = false;
		$rootScope.categoryItem = { categoryName: 'Wilma' };
		$httpBackend.expectPOST('/MarineServiceScheduler/websvc/category/add');
		$rootScope.save();
		$httpBackend.flush();
		
		expect($rootScope.message).toBeDefined();
		expect($rootScope.message.text).toBe('Service Category was created successfully');
		expect($rootScope.buttonStatus.addAnother).toBe(true);
		expect($rootScope.buttonStatus.save).toBeUndefined();
		expect($rootScope.buttonStatus.reset).toBeUndefined();
	});
	
	it("updates a valid category", function() {
		createController();
		$rootScope.isEdit = true;
		$rootScope.categoryItem = { categoryName: 'Frederick' };
		$httpBackend.expectPOST('/MarineServiceScheduler/websvc/category/update');
		$rootScope.save();
		$httpBackend.flush();
		
		expect($rootScope.message).toBeDefined();
		expect($rootScope.message.text).toBe('Service Category was updated successfully');
		expect($rootScope.buttonStatus.addAnother).toBeUndefined();
		expect($rootScope.buttonStatus.save).toBeUndefined();
		expect($rootScope.buttonStatus.reset).toBeUndefined();
	});
	
	it("sets Table Columns as needed (after getList)", function() {
		createController();
		$httpBackend.expectGET('/MarineServiceScheduler/websvc/category/list');
		$rootScope.getList(false);
		$httpBackend.flush();
		
		expect($rootScope.tableColumns).toBeDefined();
		expect($rootScope.tableColumns.length).toBe(2);
		expect($rootScope.tableColumns[0].attribute).toBe('categoryId');
		expect($rootScope.tableColumns[1].attribute).toBe('categoryName');
	});
		
	it("recognizes invalid inputs and does not save them", function() {
		createController();
		
		// adding with null
		$rootScope.isEdit = false;
		$rootScope.categoryItem = { categoryName: null };
		$rootScope.save();
		
		expect($httpBackend.flush).toThrow(); // because it was never called
		expect($rootScope.message).toBeDefined();
		expect($rootScope.message.text).toBe('The form could not be submitted due to validation errors.  Please check the form and try again.');
		
		// editing with empty string
		$rootScope.isEdit = true;
		$rootScope.categoryItem = { categoryName: '' };
		$rootScope.save();
		
		expect($httpBackend.flush).toThrow(); // because it was never called
		expect($rootScope.message).toBeDefined();
		expect($rootScope.message.text).toBe('The form could not be submitted due to validation errors.  Please check the form and try again.');
		
		// add with empty and edit with null skipped/redundant
	});
		
});