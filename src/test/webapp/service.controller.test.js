describe("Service Controller", function() {
		
	beforeEach(module('marineService'));
	
	var $httpBackend, $rootScope, $routeParams, createController;
	
	beforeEach(inject(function ($injector) {
		// Set up the mock http service responses
	    $httpBackend = $injector.get('$httpBackend');
	    
	    // Get hold of a scope (i.e. the root scope)
		$rootScope = $injector.get('$rootScope');
		
		// Set up route params
		$routeParams = $injector.get('$routeParams');
		
		// The $controller service is used to create instances of controllers
		var $controller = $injector.get('$controller');
		createController = function() {
		   return $controller('serviceCtrl', {'$scope' : $rootScope });
		};
	    
		
		// backend mock defintions
	    $httpBackend.when('GET', '/MarineServiceScheduler/websvc/service/list')
	    	.respond( [
	    	        {serviceId: 1, shortDescription: 'Flintstone', longDescription: 'A brontosaurus operator at Slate and Co.', duration: 1.0, category:{'categoryId':1,'categoryName':'Foo'}},
	    	        {serviceId: 2, shortDescription: 'Rubble', longDescription: 'Another employee of Slate and Co.', duration: 2.0, category:{'categoryId':1,'categoryName':'Foo'}},
	    	        {serviceId: 3, shortDescription: 'Dino', longDescription: 'A saurid of some sort that acts like a dog', duration: 1.5, category:{'categoryId':1,'categoryName':'Foo'}}
	    	] );
	    
	    $httpBackend.when('GET', '/MarineServiceScheduler/websvc/service/retrieve/1')
	    	.respond (
	    			{serviceId: 1, shortDescription: 'Flintstone', longDescription: 'A brontosaurus operator at Slate and Co.', duration: 1.0, category:{'categoryId':1,'categoryName':'Foo'}}
	    	);
	    
	    $httpBackend.when('POST', '/MarineServiceScheduler/websvc/service/add')
    		.respond (true);
	    $httpBackend.when('POST', '/MarineServiceScheduler/websvc/service/update')
			.respond (true);
	    $httpBackend.when('GET', '/MarineServiceScheduler/websvc/service/delete/1')
		.respond (true);

	    
	}));
		
	it("retrieves a list of services", function() {
		createController();
		$httpBackend.expectGET('/MarineServiceScheduler/websvc/service/list');
		$rootScope.getList(false);
		$httpBackend.flush();
		
		expect($rootScope.message).toBeDefined();
		expect($rootScope.message.text).toBeUndefined();
		expect($rootScope.serviceList).toBeDefined();
		expect($rootScope.serviceList.length).toBe(3);
	});
	
	it("loads an individual service", function() {
		createController();
		$httpBackend.expectGET('/MarineServiceScheduler/websvc/service/retrieve/1');
		$routeParams.serviceId = 1;
		$routeParams.state = 'edit'
		$rootScope.load();
		$httpBackend.flush();
		
		expect($rootScope.message).toBeDefined();
		expect($rootScope.message.text).toBeUndefined();
		expect($rootScope.serviceItem).toBeDefined();
		expect($rootScope.serviceItem.serviceId).toBe(1);
		expect($rootScope.serviceItem.shortDescription).toBe('Flintstone');
		expect($rootScope.serviceItem.longDescription).toBe('A brontosaurus operator at Slate and Co.');
		expect($rootScope.serviceItem.duration).toBe(1.0);
		expect($rootScope.buttonStatus).toBeDefined();
	});
	
	it("saves a new valid service", function() {
		createController();
		$rootScope.isEdit = false;
		$rootScope.serviceItem = { shortDescription:'Wilma',longDescription:'A housewife with a baby elephant for a vacuum', duration: 4.5, category:{'categoryId':1,'categoryName':'Foo'}};
		$httpBackend.expectPOST('/MarineServiceScheduler/websvc/service/add');
		$rootScope.save();
		$httpBackend.flush();
		
		expect($rootScope.message).toBeDefined();
		expect($rootScope.message.text).toBe('Service was created successfully');
		expect($rootScope.buttonStatus.addAnother).toBe(true);
		expect($rootScope.buttonStatus.save).toBeUndefined();
		expect($rootScope.buttonStatus.reset).toBeUndefined();
	});
	
	it("updates a valid service", function() {
		createController();
		$rootScope.isEdit = true;
		$rootScope.serviceItem = {serviceId: 1, shortDescription: 'Flintstone', longDescription: 'A brontosaurus operator at Slate Gravel Inc.', duration: 1.0, category:{'categoryId':1,'categoryName':'Foo'}}
		$httpBackend.expectPOST('/MarineServiceScheduler/websvc/service/update');
		$rootScope.save();
		$httpBackend.flush();
		
		expect($rootScope.message).toBeDefined();
		expect($rootScope.message.text).toBe('Service was updated successfully');
		expect($rootScope.buttonStatus.addAnother).toBeUndefined();
		expect($rootScope.buttonStatus.save).toBeUndefined();
		expect($rootScope.buttonStatus.reset).toBeUndefined();
	});
	
	it("sets Table Columns as needed (after getList)", function() {
		createController();
		$httpBackend.expectGET('/MarineServiceScheduler/websvc/service/list');
		$rootScope.getList(false);
		$httpBackend.flush();
		
		expect($rootScope.tableColumns).toBeDefined();
		expect($rootScope.tableColumns.length).toBe(4);
		expect($rootScope.tableColumns[0].attribute).toBe('serviceId');
		expect($rootScope.tableColumns[1].attribute).toBe('catName');
		expect($rootScope.tableColumns[2].attribute).toBe('shortDescription');
		expect($rootScope.tableColumns[3].attribute).toBe('duration');
		expect($rootScope.tableColumns[2].tooltip).toBe('longDescription');
		expect($rootScope.tableColumns[3].align).toBe('aln_right');
	});
		
	it("recognizes invalid inputs and does not save them", function() {
		createController();
		
		// category null
		$rootScope.isEdit = false;
		$rootScope.serviceItem = {serviceId: 1, shortDescription: 'Flintstone', longDescription: 'Valid', duration: 1.0}
		$rootScope.save();
		
		expect($httpBackend.flush).toThrow(); // because it was never called
		expect($rootScope.message).toBeDefined();
		expect($rootScope.message.text).toBe('The form could not be submitted due to validation errors.  Please check the form and try again.');
		
		// short null
		$rootScope.isEdit = true;
		$rootScope.serviceItem = {serviceId: 1, longDescription: 'Just valid', duration: 1.0, category:{'categoryId':1,'categoryName':'Foo'}}
		$rootScope.save();
		
		expect($httpBackend.flush).toThrow(); // because it was never called
		expect($rootScope.message).toBeDefined();
		expect($rootScope.message.text).toBe('The form could not be submitted due to validation errors.  Please check the form and try again.');
		
		// short empty
		$rootScope.isEdit = true;
		$rootScope.serviceItem = {serviceId: 1, shortDescription:'', longDescription: 'Just valid', duration: 1.0, category:{'categoryId':1,'categoryName':'Foo'}}
		$rootScope.save();
		
		expect($httpBackend.flush).toThrow(); // because it was never called
		expect($rootScope.message).toBeDefined();
		expect($rootScope.message.text).toBe('The form could not be submitted due to validation errors.  Please check the form and try again.');
		
		// long null
		$rootScope.isEdit = true;
		$rootScope.serviceItem = {serviceId: 1, shortDescription:'Flintstone', duration: 1.0, category:{'categoryId':1,'categoryName':'Foo'}}
		$rootScope.save();
		
		expect($httpBackend.flush).toThrow(); // because it was never called
		expect($rootScope.message).toBeDefined();
		expect($rootScope.message.text).toBe('The form could not be submitted due to validation errors.  Please check the form and try again.');
		
		// long empty
		$rootScope.isEdit = true;
		$rootScope.serviceItem = {serviceId: 1, shortDescription:'Flintstone', longDescription: '', duration: 1.0, category:{'categoryId':1,'categoryName':'Foo'}}
		$rootScope.save();
		
		expect($httpBackend.flush).toThrow(); // because it was never called
		expect($rootScope.message).toBeDefined();
		expect($rootScope.message.text).toBe('The form could not be submitted due to validation errors.  Please check the form and try again.');
		
		// duration null
		$rootScope.isEdit = true;
		$rootScope.serviceItem = {serviceId: 1, shortDescription:'Flintstone', longDescription: '', duration: null, category:{'categoryId':1,'categoryName':'Foo'}}
		$rootScope.save();
		
		expect($httpBackend.flush).toThrow(); // because it was never called
		expect($rootScope.message).toBeDefined();
		expect($rootScope.message.text).toBe('The form could not be submitted due to validation errors.  Please check the form and try again.');
		
		// duration zero
		$rootScope.isEdit = true;
		$rootScope.serviceItem = {serviceId: 1, shortDescription:'Flintstone', longDescription: '', duration: 0, category:{'categoryId':1,'categoryName':'Foo'}}
		$rootScope.save();
		
		expect($httpBackend.flush).toThrow(); // because it was never called
		expect($rootScope.message).toBeDefined();
		expect($rootScope.message.text).toBe('The form could not be submitted due to validation errors.  Please check the form and try again.');
	});
		
});