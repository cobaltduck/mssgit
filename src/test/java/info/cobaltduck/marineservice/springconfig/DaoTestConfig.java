package info.cobaltduck.marineservice.springconfig;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.ClassPathResource;

@Configuration
@ComponentScans(value = {
		@ComponentScan("info.cobaltduck.marineservice.dao")
	})
@Import(DataSourceConfig.class)
// Note the lack of WebAppConfig here or a component scan of controllers
// This config is exclusively for DAO testing- it includes datasource, but ignores WebMVC
public class DaoTestConfig {
	
	private static Logger logger = Logger.getLogger(DaoTestConfig.class);
	
	@Bean
	public PropertiesUtil propsConfigUnittest() {
		logger.info("Instantiating config for unit testing");
		PropertiesUtil propsConfig = new PropertiesUtil();
		propsConfig.setLocation(new ClassPathResource("datasource.properties"));
		return propsConfig;
	}
	
}
