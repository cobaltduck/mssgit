package info.cobaltduck.marineservice.springconfig;

import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.orm.hibernate5.HibernateTemplate;

import info.cobaltduck.marineservice.dao.ServiceCategoryDao;
import info.cobaltduck.marineservice.dao.ServiceDao;
import info.cobaltduck.marineservice.dao.TechnicianDao;

@Configuration
@ComponentScans(value = {
		@ComponentScan({"info.cobaltduck.marineservice.controller" }) })
@Import(WebConfig.class)
// Note that lack of DataSource import, and no dao package in component scan
// Basically the opposite of DaoTestConfig
public class MvcTestConfig {
	
	// the daos are introduced not through component scan, but via Mockito
	
	@Bean
	public HibernateTemplate hibernateTemplate() {
		return Mockito.mock(HibernateTemplate.class);
	}
	
	@Bean
	public ServiceCategoryDao serviceCategoryDao() {
		return Mockito.mock(ServiceCategoryDao.class);
	}
	
	@Bean
	public ServiceDao serviceDao() {
		return Mockito.mock(ServiceDao.class);
	}
	
	@Bean
	public TechnicianDao technicianDao() {
		return Mockito.mock(TechnicianDao.class);
	}

}
