package info.cobaltduck.marineservice.dao;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.transaction.annotation.Transactional;

import info.cobaltduck.marineservice.dao.ServiceCategoryDao;
import info.cobaltduck.marineservice.entity.ServiceCategory;
import info.cobaltduck.marineservice.springconfig.DaoTestConfig;

/**
 * The primary reason for this Test case is to test the Hibernate
 * and Spring configuration, not the DAO itself.  Unit testing a DAO
 * is normally not done, as it is dependent on the database and thus
 * is better done via Integration testing.
 * I'm picking on this Dao because it is simple and can stand alone without other tables/ entities
 * @author Wade J. Love
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=DaoTestConfig.class, loader=AnnotationConfigContextLoader.class)
public class TestServiceCategoryDao {

	@Autowired
	@Qualifier("serviceCategoryDao")
	private ServiceCategoryDao testDao;
	
	@Before
	public void setUp() throws Exception {
		//Server h2Server = Server.createWebServer("-web", "-webAllowOthers", "-webPort", "8082");
		//h2Server.start();
	}

	@Test
	public void testFindList() {
		List<ServiceCategory> result = testDao.findList();
		assertNotNull(result);
		assertTrue(result.size() > 0);
	}
	
	@Test
	public void testFindById() {
		ServiceCategory result = testDao.findById(1);
		assertNotNull(result);
	}
	
	@Test
	@Transactional
	public void testSave() {
		ServiceCategory testItem = new ServiceCategory();
		testItem.setCategoryName("Test Cat Name");
		testDao.save(testItem);
		List<ServiceCategory> result = testDao.findList();
		
		// test for contains
		boolean found = false;
		for (ServiceCategory cat : result) {
			if (cat.getCategoryName() == "Test Cat Name") {
				found = true;
			}
		}
		
		assertTrue(found);
	}

	/*@Test
	public void testUpdate() {
		fail("Not yet implemented");
	}

	@Test
	public void testDelete() {
		fail("Not yet implemented");
	}

	@Test
	public void testDeleteById() {
		fail("Not yet implemented");
	}*/

	

}
