package info.cobaltduck.marineservice.controller;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.AnnotationConfigWebContextLoader;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import info.cobaltduck.marineservice.dao.ServiceCategoryDao;
import info.cobaltduck.marineservice.entity.Service;
import info.cobaltduck.marineservice.entity.ServiceCategory;
import info.cobaltduck.marineservice.springconfig.MvcTestConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=MvcTestConfig.class, loader=AnnotationConfigWebContextLoader.class)
@WebAppConfiguration
public class TestCategoryController {
	
	private MockMvc mockMvc;
	
	@Autowired
	private ServiceCategoryDao mockSvcCatDao;
	
	@Autowired
    private WebApplicationContext wac;
	
	@Before
	public void setUp(){
		mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
		
		Mockito.when(mockSvcCatDao.findList()).thenReturn(buildMockList());
		Mockito.when(mockSvcCatDao.findById(Mockito.anyInt())).thenReturn(buildMockList().get(0));
	}
	
	@Test
	public void testListCategory() throws Exception {
		MvcResult result = mockMvc.perform(get("/websvc/category/list"))
			.andExpect(status().isOk())
			.andReturn();
		
		String resultStr = result.getResponse().getContentAsString();
		assertEquals(buildMockListAsJson(), resultStr);
		
		Mockito.verify(mockSvcCatDao, Mockito.times(1)).findList();
	}
	
	@Test
	public void testRetrieveCategory() throws Exception {
		MvcResult result = mockMvc.perform(get("/websvc/category/retrieve/1"))
				.andExpect(status().isOk())
				.andReturn();
		
		String resultStr = result.getResponse().getContentAsString();
		assertEquals("{\"categoryId\":1,\"categoryName\":\"Fred\",\"deletable\":true}", resultStr);
		
		Mockito.verify(mockSvcCatDao, Mockito.times(1)).findById(Mockito.anyInt());
	}
	
	@Test
	public void testUpdateCategory() throws Exception {
		String jsonParam = "{\"categoryId\":1,\"categoryName\":\"Frederick\"}";
		MvcResult result = mockMvc.perform(post("/websvc/category/update")
				.contentType(MediaType.APPLICATION_JSON)
				.content(jsonParam))
				.andExpect(status().isOk())
				.andReturn();
		
		assertEquals("true", result.getResponse().getContentAsString());
		
		Mockito.verify(mockSvcCatDao, Mockito.times(1)).update(Mockito.anyObject());
	}
	
	@Test
	public void testDeleteCategory() throws Exception {
		MvcResult result = mockMvc.perform(get("/websvc/category/delete/1"))
				.andExpect(status().isOk())
				.andReturn();
		
		assertEquals("true", result.getResponse().getContentAsString());
		
		Mockito.verify(mockSvcCatDao, Mockito.times(1)).deleteById(Mockito.anyInt());
	}
	
	@Test
	public void testAddCategory() throws Exception {
		String jsonParam = "{\"categoryId\":5,\"categoryName\":\"Dino\"}";
		MvcResult result = mockMvc.perform(post("/websvc/category/add")
				.contentType(MediaType.APPLICATION_JSON)
				.content(jsonParam))
				.andExpect(status().isOk())
				.andReturn();
		
		assertEquals("true", result.getResponse().getContentAsString());
		
		Mockito.verify(mockSvcCatDao, Mockito.times(1)).save(Mockito.anyObject());
	}
	
	private List<ServiceCategory> buildMockList() {
		ServiceCategory s1 = new ServiceCategory();
		ServiceCategory s2 = new ServiceCategory();
		ServiceCategory s3 = new ServiceCategory();
		
		s1.setCategoryId(1);
		s1.setCategoryName("Fred");
		s1.getServices();
		s2.setCategoryId(2);
		s2.setCategoryName("Wilma");
		s2.getServices().add(new Service());
		s3.setCategoryId(3);
		s3.setCategoryName("Barney");
		s3.getServices();
		
		List<ServiceCategory> cats =  new ArrayList<ServiceCategory>();
		cats.add(s1);
		cats.add(s2);
		cats.add(s3);
		
		return cats;
	}
	
	private String buildMockListAsJson() {
		return "[{\"categoryId\":1,\"categoryName\":\"Fred\",\"deletable\":true},"
				+ "{\"categoryId\":2,\"categoryName\":\"Wilma\",\"deletable\":false},"
				+ "{\"categoryId\":3,\"categoryName\":\"Barney\",\"deletable\":true}]";
	}
}
