package info.cobaltduck.marineservice.controller;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.AnnotationConfigWebContextLoader;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import info.cobaltduck.marineservice.dao.ServiceDao;
import info.cobaltduck.marineservice.entity.Service;
import info.cobaltduck.marineservice.entity.ServiceCategory;
import info.cobaltduck.marineservice.springconfig.MvcTestConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=MvcTestConfig.class, loader=AnnotationConfigWebContextLoader.class)
@WebAppConfiguration
public class TestServiceController {
	
	private MockMvc mockMvc;
	
	@Autowired
	private ServiceDao mockServiceDao;
	
	@Autowired
    private WebApplicationContext wac;
	
	@Before
	public void setUp(){
		mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
		
		Mockito.when(mockServiceDao.findList()).thenReturn(buildMockList());
		Mockito.when(mockServiceDao.findById(Mockito.anyInt())).thenReturn(buildMockList().get(0));
	}
	
	@Test
	public void testListService() throws Exception {
		MvcResult result = mockMvc.perform(get("/websvc/service/list"))
			.andExpect(status().isOk())
			.andReturn();
		
		String resultStr = result.getResponse().getContentAsString();
		assertEquals(buildMockListAsJson(), resultStr);
		
		Mockito.verify(mockServiceDao, Mockito.times(1)).findList();
	}
	
	@Test
	public void testRetrieveService() throws Exception {
		MvcResult result = mockMvc.perform(get("/websvc/service/retrieve/1"))
				.andExpect(status().isOk())
				.andReturn();
		
		String resultStr = result.getResponse().getContentAsString();
		String expect = "{\"serviceId\":1,\"shortDescription\":\"Sewing\",\"longDescription\":\"Running a needle and thread over and under pieces of fabric to secure them together\","
				+ "\"duration\":2.0,\"category\":{\"categoryId\":1,\"categoryName\":\"Bright Work\",\"deletable\":true},\"deletable\":true}";
		assertEquals(expect, resultStr);
		
		Mockito.verify(mockServiceDao, Mockito.times(1)).findById(Mockito.anyInt());
	}
	
	@Test
	public void testUpdateService() throws Exception {
		String jsonParam = "{\"serviceId\":1,\"shortDescription\":\"Finger Poking\"}";
		MvcResult result = mockMvc.perform(post("/websvc/service/update")
				.contentType(MediaType.APPLICATION_JSON)
				.content(jsonParam))
				.andExpect(status().isOk())
				.andReturn();
		
		assertEquals("true", result.getResponse().getContentAsString());
		
		Mockito.verify(mockServiceDao, Mockito.times(1)).update(Mockito.anyObject());
	}
	
	@Test
	public void testDeleteService() throws Exception {
		MvcResult result = mockMvc.perform(get("/websvc/service/delete/1"))
				.andExpect(status().isOk())
				.andReturn();
		
		assertEquals("true", result.getResponse().getContentAsString());
		
		Mockito.verify(mockServiceDao, Mockito.times(1)).deleteById(Mockito.anyInt());
	}
	
	@Test
	public void testAddService() throws Exception {
		String jsonParam = "{\"serviceId\":6,\"shortDescription\":\"Coiling\"}";
		MvcResult result = mockMvc.perform(post("/websvc/service/add")
				.contentType(MediaType.APPLICATION_JSON)
				.content(jsonParam))
				.andExpect(status().isOk())
				.andReturn();
		
		assertEquals("true", result.getResponse().getContentAsString());
		
		Mockito.verify(mockServiceDao, Mockito.times(1)).save(Mockito.anyObject());
	}
	
	private List<Service> buildMockList() {
		ServiceCategory c1 = new ServiceCategory();
		ServiceCategory c2 = new ServiceCategory();
		c1.setCategoryId(1);
		c1.setCategoryName("Bright Work");
		c2.setCategoryId(2);
		c2.setCategoryName("Deck Seamanship");
		
		Service s1 = new Service();
		Service s2 = new Service();
		Service s3 = new Service();
		Service s4 = new Service();
		Service s5 = new Service();
		
		s1.setServiceId(1);
		s1.setDuration(2.0);
		s1.setShortDescription("Sewing");
		s1.setLongDescription("Running a needle and thread over and under pieces of fabric to secure them together");
		s1.setCategory(c1);
		s2.setServiceId(2);
		s2.setDuration(1.5);
		s2.setShortDescription("Macrame");
		s2.setLongDescription("Repeatedly tying knots to form a decorative object");
		s2.setCategory(c1);
		s3.setServiceId(3);
		s3.setDuration(1.0);
		s3.setShortDescription("Braiding");
		s3.setLongDescription("Inter-weaving strands of rope or hair together in a symmetrical pattern");
		s3.setCategory(c1);
		s4.setServiceId(4);
		s4.setDuration(0.5);
		s4.setShortDescription("Flemmishing");
		s4.setLongDescription("Laying the unused portion of a line down on the deck in a spiral mat");
		s4.setCategory(c2);
		s5.setServiceId(5);
		s5.setDuration(2.0);
		s5.setShortDescription("Faking");
		s5.setLongDescription("Laying the unused portion of a line down on the deck in a series of overlapping loops");
		s5.setCategory(c2);
		
		List<Service> svcs =  new ArrayList<Service>();
		svcs.add(s1);
		svcs.add(s2);
		svcs.add(s3);
		svcs.add(s4);
		svcs.add(s5);
		
		return svcs;
	}
	
	private String buildMockListAsJson() {
		return "[{\"serviceId\":1,\"shortDescription\":\"Sewing\",\"longDescription\":\"Running a needle and thread over and under pieces of fabric to secure them together\","
				+ "\"duration\":2.0,\"category\":{\"categoryId\":1,\"categoryName\":\"Bright Work\",\"deletable\":true},\"deletable\":true},"
				+ "{\"serviceId\":2,\"shortDescription\":\"Macrame\",\"longDescription\":\"Repeatedly tying knots to form a decorative object\","
				+ "\"duration\":1.5,\"category\":{\"categoryId\":1,\"categoryName\":\"Bright Work\",\"deletable\":true},\"deletable\":true},"
				+ "{\"serviceId\":3,\"shortDescription\":\"Braiding\",\"longDescription\":\"Inter-weaving strands of rope or hair together in a symmetrical pattern\","
				+ "\"duration\":1.0,\"category\":{\"categoryId\":1,\"categoryName\":\"Bright Work\",\"deletable\":true},\"deletable\":true},"
				+ "{\"serviceId\":4,\"shortDescription\":\"Flemmishing\",\"longDescription\":\"Laying the unused portion of a line down on the deck in a spiral mat\","
				+ "\"duration\":0.5,\"category\":{\"categoryId\":2,\"categoryName\":\"Deck Seamanship\",\"deletable\":true},\"deletable\":true},"
				+ "{\"serviceId\":5,\"shortDescription\":\"Faking\",\"longDescription\":\"Laying the unused portion of a line down on the deck in a series of overlapping loops\","
				+ "\"duration\":2.0,\"category\":{\"categoryId\":2,\"categoryName\":\"Deck Seamanship\",\"deletable\":true},\"deletable\":true}]";
				
	}

}
