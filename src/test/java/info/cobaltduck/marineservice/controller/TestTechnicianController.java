package info.cobaltduck.marineservice.controller;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.AnnotationConfigWebContextLoader;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import info.cobaltduck.marineservice.dao.TechnicianDao;
import info.cobaltduck.marineservice.entity.Technician;
import info.cobaltduck.marineservice.springconfig.MvcTestConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=MvcTestConfig.class, loader=AnnotationConfigWebContextLoader.class)
@WebAppConfiguration
public class TestTechnicianController {

	private MockMvc mockMvc;
	
	@Autowired
	private TechnicianDao mockTechDao;
	
	@Autowired
    private WebApplicationContext wac;
	
	@Before
	public void setUp(){
		mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
		
		Mockito.when(mockTechDao.findList()).thenReturn(buildMockList());
		Mockito.when(mockTechDao.findById(Mockito.anyInt())).thenReturn(buildMockList().get(0));
	}
	
	@Test
	public void testListTechnician() throws Exception {
		MvcResult result = mockMvc.perform(get("/websvc/technician/list"))
			.andExpect(status().isOk())
			.andReturn();
		
		String resultStr = result.getResponse().getContentAsString();
		assertEquals(buildMockListAsJson(), resultStr);
		
		Mockito.verify(mockTechDao, Mockito.times(1)).findList();
	}
	
	@Test
	public void testRetrieveTechnician() throws Exception {
		MvcResult result = mockMvc.perform(get("/websvc/technician/retrieve/1"))
				.andExpect(status().isOk())
				.andReturn();
		
		String resultStr = result.getResponse().getContentAsString();
		String expect = "{\"technicianId\":1,\"user\":null,\"firstname\":\"Scooby\",\"lastname\":\"Doo\",\"breakhour\":1507651200000,\"deletable\":true}";
		assertEquals(expect, resultStr);
		
		Mockito.verify(mockTechDao, Mockito.times(1)).findById(Mockito.anyInt());
	}
	
	@Test
	public void testUpdateTechnician() throws Exception {
		String jsonParam = "{\"technicianId\":1,\"firstname\":\"Scoobert\"}";
		MvcResult result = mockMvc.perform(post("/websvc/technician/update")
				.contentType(MediaType.APPLICATION_JSON)
				.content(jsonParam))
				.andExpect(status().isOk())
				.andReturn();
		
		assertEquals("true", result.getResponse().getContentAsString());
		
		Mockito.verify(mockTechDao, Mockito.times(1)).update(Mockito.anyObject());
	}
	
	@Test
	public void testDeleteTechnician() throws Exception {
		MvcResult result = mockMvc.perform(get("/websvc/technician/delete/1"))
				.andExpect(status().isOk())
				.andReturn();
		
		assertEquals("true", result.getResponse().getContentAsString());
		
		Mockito.verify(mockTechDao, Mockito.times(1)).deleteById(Mockito.anyInt());
	}
	
	@Test
	public void testAddTechnician() throws Exception {
		String jsonParam = "{\"technicianId\":5,\"firstname\":\"Velma\"}";
		MvcResult result = mockMvc.perform(post("/websvc/technician/add")
				.contentType(MediaType.APPLICATION_JSON)
				.content(jsonParam))
				.andExpect(status().isOk())
				.andReturn();
		
		assertEquals("true", result.getResponse().getContentAsString());
		
		Mockito.verify(mockTechDao, Mockito.times(1)).save(Mockito.anyObject());
	}
	
	private List<Technician> buildMockList() {
		Technician t1 = new Technician();
		Technician t2 = new Technician();
		@SuppressWarnings("deprecation")  // Building this date only for this test
		Date noon = new Date("10/10/2017 12:00");
		//noon.setHours(12);
		
		t1.setTechnicianId(1);
		t1.setFirstname("Scooby");
		t1.setLastname("Doo");
		t1.setBreakhour(noon);
		t2.setTechnicianId(2);
		t2.setFirstname("Shaggy");
		t2.setLastname("Rogers");
		t2.setBreakhour(noon);
		
		
		List<Technician> techs =  new ArrayList<Technician>();
		techs.add(t1);
		techs.add(t2);
		
		return techs;
	}
	
	private String buildMockListAsJson() {
		return "[{\"technicianId\":1,\"user\":null,\"firstname\":\"Scooby\",\"lastname\":\"Doo\",\"breakhour\":1507651200000,\"deletable\":true},"
				+ "{\"technicianId\":2,\"user\":null,\"firstname\":\"Shaggy\",\"lastname\":\"Rogers\",\"breakhour\":1507651200000,\"deletable\":true}]";
	}
}
