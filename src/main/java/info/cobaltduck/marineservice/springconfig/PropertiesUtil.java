package info.cobaltduck.marineservice.springconfig;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

/**
 * Utility to read properties from .props files.
 * All credit here goes to: https://stackoverflow.com/a/6817902/241291
 * This class sub-classes Spring's own Properties reader in a way that
 * actually allows those properties to be used downstream.  (Now why
 * isn't this behavior built-in?)
 */
public class PropertiesUtil extends PropertyPlaceholderConfigurer {

	private static Map<String, String> propertiesMap;
    // Default as in PropertyPlaceholderConfigurer
    private int springSystemPropertiesMode = SYSTEM_PROPERTIES_MODE_FALLBACK;

    @Override
    public void setSystemPropertiesMode(int systemPropertiesMode) {
        super.setSystemPropertiesMode(systemPropertiesMode);
        springSystemPropertiesMode = systemPropertiesMode;
    }

    @Override
    protected void processProperties(ConfigurableListableBeanFactory beanFactory, Properties props)
    		throws BeansException {
        super.processProperties(beanFactory, props);

        propertiesMap = new HashMap<String, String>();
        for (Object key : props.keySet()) {
            String keyStr = key.toString();
            String valueStr = resolvePlaceholder(keyStr, props, springSystemPropertiesMode);
            propertiesMap.put(keyStr, valueStr);
        }
    }

    /**
     * The reason this class is needed.  Call statically to get the value
     * of a property from the props files.
     * @param key
     * @return value corresponding to key in the props files
     */
    public static String getProperty(String key) {
    	// Most property setters will throw exception if sent an empty string.
    	// This avoids NPE here and gives a more informative exception later.
    	if(propertiesMap.get(key) != null) {
    		return propertiesMap.get(key).toString();
    	}
    	else {
    		return "";
    	}
    }
    
}
