/**
 * Package contains annotated classes for configuring Spring beans and aspects
 * @author Wade J. Love
 */
package info.cobaltduck.marineservice.springconfig;