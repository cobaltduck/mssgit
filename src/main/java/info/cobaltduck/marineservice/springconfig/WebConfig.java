package info.cobaltduck.marineservice.springconfig;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

/**
 * Spring configuration for the WebMVC portions of this application.
 * Performs the equivalent of {@code @EnableWebMVC} so that {@code @Controller} items are
 * correctly identified as servlets, as well as adding an override that
 * set alwaysUseFullPath to true, allowing the servlet url mapping we want.
 * 
 * @author Wade J. Love
 */
@Configuration
public class WebConfig  extends WebMvcConfigurationSupport{
	// extending Support means we get @EnableWebMvc auto-magically, and is required for override below
	
	/* Override allows using "/websvc/*" as our url-pattern in web.xml, and thus
	 * angular pages are ignored by the servlet.  Credit for this is due to:
	 * http://forum.spring.io/forum/spring-projects/web/112412-spring-3-1-enablewebmvc-setting-alwaysusefullpath-property
	 */
	@Override
	public RequestMappingHandlerMapping requestMappingHandlerMapping() {
		RequestMappingHandlerMapping handlerMapping = super.requestMappingHandlerMapping();
		handlerMapping.setAlwaysUseFullPath(true);
		return handlerMapping;
	}
	
	// another requirement: for the WebMVC controllers to return JSON, need pom dependencies on BOTH 
	// codehaus jackson-mapper-asl and fasterxml jackson-databind.  After that, is auto-magic.
	
}
