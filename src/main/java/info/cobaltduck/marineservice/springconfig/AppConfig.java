package info.cobaltduck.marineservice.springconfig;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.ClassPathResource;

/**
 * Spring Configuration for the main application (The One Config to Rule Them All).
 * Defines the property file based on the active profile. Imports the Data Source Config
 * and Web Config, and sets up component scans to capture beans in other packages.
 * Finally, defines the aspects for logging (and in future, security).
 * 
 * @author Wade J. Love
 */
@Configuration
@ComponentScans(value = {
		@ComponentScan({ "info.cobaltduck.marineservice.dao",
			"info.cobaltduck.marineservice.controller" }) })
@EnableAspectJAutoProxy
@Import({ DataSourceConfig.class, WebConfig.class })
public class AppConfig {
		
	private static Logger logger = Logger.getLogger(AppConfig.class);

	@Bean
	@Profile("localhost")
	public PropertiesUtil propsConfigLocalhost() {
		logger.info("Instantiating config with localhost profile");
		PropertiesUtil propsConfig = new PropertiesUtil();
		propsConfig.setLocation(new ClassPathResource("datasource.properties"));
		return propsConfig;
	}

	@Bean
	@Profile("cobaltduck")
	public PropertiesUtil propsConfig() {
		logger.info("Instantiating config with cobaltduck (deployment) profile");
		PropertiesUtil propsConfig = new PropertiesUtil();
		propsConfig.setLocation(new ClassPathResource("cobaltduck/datasource.properties"));
		return propsConfig;
	}

	@Bean
	public LoggingAspect loggingAspect() {
		return new LoggingAspect();
	}

}
