package info.cobaltduck.marineservice.springconfig;

import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Spring Configuration for the data source and Hibernate
 * Session and Template beans.
 * 
 * @author Wade J. Love
 */
@Configuration
@EnableTransactionManagement
public class DataSourceConfig {
	
	@Bean
	public DataSource dataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		
		dataSource.setDriverClassName(PropertiesUtil.getProperty("datasource.driver"));
		dataSource.setUrl(PropertiesUtil.getProperty("datasource.url"));
		dataSource.setUsername(PropertiesUtil.getProperty("datasource.username"));
		dataSource.setPassword(PropertiesUtil.getProperty("datasource.password"));
		
		return dataSource;
	}
	
	@Bean
	public LocalSessionFactoryBean sessionFactory() {
		LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
		
		sessionFactory.setDataSource(dataSource());
		sessionFactory.setPackagesToScan("info.cobaltduck.marineservice.entity");
		sessionFactory.setHibernateProperties(hibernateProps());
		
		return sessionFactory;
	}
	
	@Bean
	public HibernateTemplate hibernateTemplate() {
		HibernateTemplate template = new HibernateTemplate();
		template.setSessionFactory(sessionFactory().getObject());
		template.setCheckWriteOperations(false);
		
		return template;
	}
	
	@Bean
	public HibernateTransactionManager hibernateTransactionManager() {
		HibernateTransactionManager txMan = new HibernateTransactionManager();
		txMan.setSessionFactory(sessionFactory().getObject());
		return txMan;
	}
	
	private Properties hibernateProps() {
		Properties props = new Properties();
		props.put("hibernate.dialect", PropertiesUtil.getProperty("datasource.hibernate.dialect"));
		props.put("hibernate.hbm2ddl.auto", PropertiesUtil.getProperty("datasrouce.hibernate.auto"));
		props.put("hibernate.show_sql", PropertiesUtil.getProperty("datasource.hibernate.showsql"));
		
		return props;
	}

}
