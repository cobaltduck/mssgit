package info.cobaltduck.marineservice.springconfig;

import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

/**
 * Log4j aspect for DAO classes and Controller classes.
 * Places all logging information in one class, and frees
 * individual classes from doing their own logging.
 * 
 * @author Wade J. Love
 *
 */
@Aspect
@Component
public class LoggingAspect {
	
	private static Logger logger = Logger.getLogger(LoggingAspect.class);
	
	@Pointcut("execution(* info.cobaltduck.marineservice.dao.AbstractDao+.*(..))")
	private void daoAllMethodPointcut(){}
	
	@Pointcut("execution(* info.conbaltduck.marineservice.controller..*(..))")
	private void controllerPackagePointcut(){}
	
	@Before("daoAllMethodPointcut()")
	public void logDaoMethodCalls(JoinPoint jp) {
		logger.info("Calling Dao target... " + jp.getTarget());
		logger.info("Calling Dao method... " + jp.toShortString());
		if(jp.getArgs() != null && logger.isDebugEnabled()) {
			logger.debug("Dao Method argument... " + jp.getArgs()[0]);
		}
	}
	
	@AfterReturning(pointcut="daoAllMethodPointcut()", returning="result")
	public void logDaoResults(JoinPoint jp, Object result) {
		logger.info("Succesful return from Dao target... " + jp.getTarget());
		logger.info("Successful return from Dao method... " +jp.toShortString());
		if(result != null && logger.isDebugEnabled()) {
			logger.debug("Dao method result... " + result);
		}
	}
	
	@AfterThrowing(pointcut="daoAllMethodPointcut()", throwing="ex")
	public void logDaoExceptions(Throwable ex) {
		logger.error("Dao method threw exception... ");
		logger.error(ex.getStackTrace());
	}
	
	@AfterThrowing(pointcut="controllerPackagePointcut()", throwing="ex")
	public void logControllerExceptions(Throwable ex) {
		logger.error("Controller method threw exception... ");
		logger.error(ex.getStackTrace());
	}

}
