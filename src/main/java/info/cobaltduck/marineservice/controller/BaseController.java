package info.cobaltduck.marineservice.controller;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;

import info.cobaltduck.marineservice.dao.AbstractDao;
import info.cobaltduck.marineservice.entity.IMarineServiceEntity;

/**
 * A parent WebMVC controller that allows other controllers to be D.R.Y.
 * Performs the basic repetitive DAO calls that all controller will perform-
 * the child controller only needs to specify which entity and dao are involved,
 * and its base RequestMapping url pattern.
 *  
 * @author Wade J. Love
 *
 * @param <T>  The specific IMarineServiceEntity used by the child
 * @param <U>  The specific AbstactDao used by the child
 */
public abstract class BaseController<T extends IMarineServiceEntity, U extends AbstractDao<T>> {
	
	protected U dao;
	private Class<T> type;
	
	protected BaseController(Class<T> type, U dao) {
		this.type = type;
		this.dao = dao;
	}
	
	@RequestMapping(value="/list",
			method=RequestMethod.GET, produces="application/json")
	public @ResponseBody List<T> list() {
		List<T> result = dao.findList();
		return result;
	}
	
	@RequestMapping(value="/retrieve/{itemId}",
			method=RequestMethod.GET, produces="application/json")
	public @ResponseBody T retrieve(@PathVariable int itemId) {
		T result = dao.findById(itemId);
		return result;
	}
	
	@RequestMapping(value="/update",
			method=RequestMethod.POST)
	@Transactional
	public @ResponseBody boolean update(@RequestBody String json) throws Exception {
		T itemToUpdate = new ObjectMapper().readValue(json, type);
		dao.update(itemToUpdate);
		return true;
	}
	
	@RequestMapping(value="/add",
			method=RequestMethod.POST)
	@Transactional
	public @ResponseBody boolean add(@RequestBody String json) throws Exception {
		T itemToAdd = new ObjectMapper().readValue(json, type);
		dao.save(itemToAdd);
		return true;
	}
	
	@RequestMapping(value="/delete/{itemId}",
			method=RequestMethod.GET)
	@Transactional
	public @ResponseBody boolean deleteCategory(@PathVariable int itemId) {
		dao.deleteById(itemId);
		return true;
	}

	/* 
	 * Note that the boolean methods can never return false- if an exception is thrown they return
	 * nothing at all.  The true returns are a verification for the angular front-end.
	 * The exceptions are "caught" and logged in the aspect. 
	 */
	
}
