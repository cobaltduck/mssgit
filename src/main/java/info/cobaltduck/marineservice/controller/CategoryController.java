package info.cobaltduck.marineservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import info.cobaltduck.marineservice.dao.ServiceCategoryDao;
import info.cobaltduck.marineservice.entity.ServiceCategory;

@Controller
@RequestMapping(value="/websvc/category")
/**
 * WebMVC controller for Service Category.
 * 
 * @author Wade J. Love
 */
public class CategoryController extends BaseController<ServiceCategory, ServiceCategoryDao> {
	
	@Autowired
	public CategoryController(ServiceCategoryDao svcCatDao) {
		super(ServiceCategory.class, svcCatDao);
	}

}