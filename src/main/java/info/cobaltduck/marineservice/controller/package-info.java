/**
 * Package contains web controller classes
 * @author Wade J. Love
 */
package info.cobaltduck.marineservice.controller;