package info.cobaltduck.marineservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import info.cobaltduck.marineservice.dao.ServiceDao;
import info.cobaltduck.marineservice.entity.Service;

@Controller
@RequestMapping(value="/websvc/service")
/**
 * WebMVC controller for Service.
 * 
 * @author Wade J. Love
 */
public class ServiceController extends BaseController<Service, ServiceDao> {
	
	@Autowired
	public ServiceController(ServiceDao serviceDao) {
		super(Service.class, serviceDao);
	}

}