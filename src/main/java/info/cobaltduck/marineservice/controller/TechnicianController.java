package info.cobaltduck.marineservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import info.cobaltduck.marineservice.dao.TechnicianDao;
import info.cobaltduck.marineservice.entity.Technician;

@Controller
@RequestMapping(value="/websvc/technician")
/**
 * WebMVC controller for Technician.
 * 
 * @author Wade J. Love
 */
public class TechnicianController extends BaseController<Technician, TechnicianDao> {

	@Autowired
	public TechnicianController(TechnicianDao technicianDao) {
		super(Technician.class, technicianDao);
	}

}