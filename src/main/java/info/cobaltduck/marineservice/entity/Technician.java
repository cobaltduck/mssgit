package info.cobaltduck.marineservice.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

/**
 * Hibernate entity for Technician.
 * A technician is an employee of the shop who is trained (and paid) to perform
 * services on vessels.
 * 
 * @author Wade J. Love
 *
 */
@Entity
@Table(name = "technician")
public class Technician implements IMarineServiceEntity {
	
	@Id
	@Column(name = "technician_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int technicianId;
	
	@OneToOne(cascade = CascadeType.ALL, optional = false)
	@JoinColumn(name = "user_id", referencedColumnName = "user_id")
	private UserLogin user;
	
	@Column(name = "firstname")
	private String firstname;
	
	@Column(name = "lastname")
	private String lastname;
	
	@Temporal(TemporalType.TIME)
	@Column(name = "breakhour")
	private Date breakhour; // Note: java.util.Date works ok for this

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "technician")
	@JsonIgnore
	private List<Appointment> appointments;
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "skill",
		joinColumns = {@JoinColumn(name = "technician_id")},
		inverseJoinColumns = {@JoinColumn(name = "service_id")}
	)
	@JsonIgnore
	private List<Service> skills;
	
	
	public int getTechnicianId() {
		return technicianId;
	}

	public void setTechnicianId(int technicianId) {
		this.technicianId = technicianId;
	}

	public UserLogin getUser() {
		return user;
	}

	public void setUser(UserLogin user) {
		this.user = user;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public Date getBreakhour() {
		return breakhour;
	}

	public void setBreakhour(Date breakhour) {
		this.breakhour = breakhour;
	}
	
	public List<Service> getSkills() {
		if (skills == null) {
			skills = new ArrayList<Service>();
		}
		return skills;
	}
	
	public List<Appointment> getAppointments() {
		if(appointments == null) {
			appointments = new ArrayList<Appointment>();
		}
		return appointments;
	}
	
	// Note lack of setter for collections- preferred use is getList().addAll(otherList)
	
	@Override
	@JsonGetter("deletable")
	@JsonProperty(access = Access.READ_ONLY)
	public boolean isDeletable() {
		return true;
		// TODO: a technician can be deleted only when it has no associated appointments
	}
	
	@Override
	public int hashCode() {
		return HASH_PRIME * HASH_SEED + technicianId;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Technician other = (Technician) obj;
		if (technicianId != other.technicianId)
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("Technician Entity [");
		sb.append("id = ").append(Integer.toString(technicianId)).append(", ");
		sb.append("First = ").append(firstname).append(", ");
		sb.append("Last = ").append(lastname).append(" ]");
		return sb.toString();
	}
	
}
