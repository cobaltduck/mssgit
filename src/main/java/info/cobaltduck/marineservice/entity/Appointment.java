package info.cobaltduck.marineservice.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

/**
 * Hibernate entity for Appointment.
 * An appointment matches a vessel, a service to be performed on that vessel, and the
 * technician who will perform that service, at a specified start and end time.
 * 
 * @author Wade J. Love
 *
 */
@Entity
@Table(name = "appointment")
public class Appointment implements IMarineServiceEntity {

	@Id
	@Column(name = "appointment_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int appointmentId;
	
	@ManyToOne
	@JoinColumn(name = "vessel_id")
	private Vessel vessel;
	
	@ManyToOne
	@JoinColumn(name = "service_id")
	private Service service;
	
	@ManyToOne
	@JoinColumn(name = "technician_id")
	private Technician technician;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "state_time")
	private Date startTime;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "end_time")
	private Date endTime;

	
	public int getAppointmentId() {
		return appointmentId;
	}

	public void setAppointmentId(int appointmentId) {
		this.appointmentId = appointmentId;
	}

	public Vessel getVessel() {
		return vessel;
	}

	public void setVessel(Vessel vessel) {
		this.vessel = vessel;
	}

	public Service getService() {
		return service;
	}

	public void setService(Service service) {
		this.service = service;
	}

	public Technician getTechnician() {
		return technician;
	}

	public void setTechnician(Technician technician) {
		this.technician = technician;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	
	@Override
	@JsonGetter("deletable")
	@JsonProperty(access = Access.READ_ONLY)
	public boolean isDeletable() {
		return true; // an appointment can always be deleted
	}

	@Override
	public int hashCode() {
		return HASH_PRIME * HASH_SEED + appointmentId;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Appointment other = (Appointment) obj;
		if (appointmentId != other.appointmentId)
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("Appointment Entity [");
		sb.append("id = ").append(Integer.toString(appointmentId)).append(", ");
		sb.append("Vessel = ").append(vessel.getVesselName()).append(", ");
		sb.append("Technician = ").append(Integer.toString(technician.getTechnicianId())).append(", ");
		sb.append("Start = ").append(startTime.toString()).append(", ");
		sb.append("End = ").append(endTime.toString()).append(" ]");
		return sb.toString();
	}
		
}
