package info.cobaltduck.marineservice.entity;

/**
 * A marker interface to enforce types in DAO
 * @author Wade J. Love
 */
public interface IMarineServiceEntity {
	// It has been determined this does NOT need to extend Serializable
	
	public static final int HASH_SEED = 3;
	public static final int HASH_PRIME = 31;
	
	/**
	 * Each implementer determines when it is valid to allow deleting itself from
	 * the database, typically if there are no potential constraint violations.
	 * Allows the UI to know whether to allow delete.
	 * Implementer must mark this method as {@code @JsonGetter} and the class
	 * as {@code @JsonIgnoreProperties(ignoreUnknown = true)}
	 * @return whether it is safe to delete the given row from database
	 */
	public boolean isDeletable();
	
}
