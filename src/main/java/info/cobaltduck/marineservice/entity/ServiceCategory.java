package info.cobaltduck.marineservice.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

/**
 * Hibernate Entity class for Service Category.
 * Organizing services into categories aids in searching for
 * a specific service.
 * 
 * @author Wade J. Love
 *
 */
@Entity
@Table(name = "service_category")
public class ServiceCategory implements IMarineServiceEntity {
	
	@Id
	@Column(name = "category_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int categoryId;
	
	@Column(name = "category_name")
	private String categoryName;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "category")
	@JsonIgnore
	private List<Service> services;
	
	
	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	
	public List<Service> getServices() {
		if (services == null) {
			services = new ArrayList<Service>();
		}
		return services;
	}
	
	// Note lack of setter for collections- preferred use is getList().addAll(otherList)
	
	@Override
	@JsonGetter("deletable")
	@JsonProperty(access = Access.READ_ONLY)
	public boolean isDeletable() {
		return this.getServices().isEmpty(); 
		// a category can be deleted ony if it has no associated services
		// note that this required the fetch of services to be EAGER.
	}
	
	@Override
	public int hashCode() {
		return HASH_PRIME * HASH_SEED + categoryId;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ServiceCategory other = (ServiceCategory) obj;
		if (categoryId != other.categoryId)
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("Service Category Entity [");
		sb.append("id = ").append(Integer.toString(categoryId)).append(", ");
		sb.append("Name = ").append(categoryName).append(" ]");
		return sb.toString();
	}
	
}
