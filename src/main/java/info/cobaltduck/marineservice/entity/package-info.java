/**
 * Package contains Hibernate entity classes for the application
 * @author Wade J. Love
 */
package info.cobaltduck.marineservice.entity;