package info.cobaltduck.marineservice.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

/**
 * Hibernate entity for Customer.
 * A customer owns a vessel and requests that a service be performed upon
 * that vessel.
 * 
 * @author Wade J. Love
 *
 */
@Entity
@Table(name = "customer")
public class Customer implements IMarineServiceEntity {
	
	@Id
	@Column(name = "customer_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int customerId;
	
	@OneToOne
	@JoinColumn(name = "user_id")
	private UserLogin user;	// optional, only if customer is registered 
	
	@Column(name = "firstname")
	private String firstName;
	
	@Column(name = "lastname")
	private String lastName;
	
	@Column(name = "addr_line1")
	private String addressLine1;
	
	@Column(name = "addr_line2")
	private String addressline2;
	
	@Column(name = "addr_city")
	private String city;
	
	@Column(name = "addr_state")
	private String state;
	
	@Column(name = "addr_zip")
	private String zip;
	
	@Column(name = "phone")
	private String phone;
	
	@Column(name = "email")
	private String email;
	
	@Column(name = "prefer_email")
	private boolean preferEmail; // if true, customer prefers email; false, customer prefers phone

	
	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public UserLogin getUser() {
		return user;
	}

	public void setUser(UserLogin user) {
		this.user = user;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressline2() {
		return addressline2;
	}

	public void setAddressline2(String addressline2) {
		this.addressline2 = addressline2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isPreferEmail() {
		return preferEmail;
	}

	public void setPreferEmail(boolean preferEmail) {
		this.preferEmail = preferEmail;
	}
	
	@Override
	@JsonGetter("deletable")
	@JsonProperty(access = Access.READ_ONLY)
	public boolean isDeletable() {
		return true;
		// TODO: A customer can be deleted only if it has no associated vessels
	}
	
	@Override
	public int hashCode() {
		return HASH_PRIME * HASH_SEED + customerId;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Customer other = (Customer) obj;
		if (customerId != other.customerId)
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("Customer Entity [");
		sb.append("id = ").append(Integer.toString(customerId)).append(", ");
		sb.append("First = ").append(firstName).append(", ");
		sb.append("Last = ").append(lastName).append(" ]");
		return sb.toString();
	}
	
}
