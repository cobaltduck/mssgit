package info.cobaltduck.marineservice.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

/**
 * Hibernate entity for a user's login.
 * Some customers and all managers and technicians must enter
 * a username and password to access proper features of the application.
 * 
 * @author Wade J. Love
 *
 */
@Entity
@Table(name = "user_login")
public class UserLogin implements IMarineServiceEntity {
	
	private enum UserLoginType {
		MANAGER,
		TECHNICIAN,
		CUSTOMER
	}
	
	@Id
	@Column(name = "user_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int userId;
	
	@Column(name = "username")
	private String username;
	
	@Column(name = "password")
	@JsonProperty(access = Access.WRITE_ONLY)
	private String password;
	
	@Column(name = "user_type")
	@Enumerated(EnumType.STRING) // note: is case-sensitive
	private UserLoginType userType;

	
	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public UserLoginType getUserType() {
		return userType;
	}

	public void setUserType(UserLoginType userType) {
		this.userType = userType;
	}
	
	@Override
	@JsonGetter("deletable")
	@JsonProperty(access = Access.READ_ONLY)
	public boolean isDeletable() {
		return true;
		// TODO: a user login can be deleted only if it has no assoiciated customer nor technician
	}
	
	@Override
	public int hashCode() {
		return HASH_PRIME * HASH_SEED + userId;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserLogin other = (UserLogin) obj;
		if (userId != other.userId)
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("User Login Entity [");
		sb.append("id = ").append(Integer.toString(userId)).append(", ");
		sb.append("username = ").append(username).append(" ]");
		return sb.toString();
	}
	
}
