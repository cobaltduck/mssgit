package info.cobaltduck.marineservice.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

/**
 * Hibernate Entity for Service.
 * A service is an individual repair, maintenance, or similar task
 * that a technician performs on a vessel as requested by a customer.
 *
 * @author Wade J. Love
 *
 */
@Entity
@Table(name = "service")
public class Service implements IMarineServiceEntity {
	
	@Id
	@Column(name = "service_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int serviceId;
	
	@Column(name = "short_desc")
	private String shortDescription;
	
	@Column(name = "long_desc")
	private String longDescription;
	
	@Column(name = "duration")
	private double duration;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "category")
	private ServiceCategory category;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "skill",
		joinColumns = {@JoinColumn(name = "service_id")},
		inverseJoinColumns = {@JoinColumn(name = "technician_id")}
	)
	@JsonIgnore
	private List<Technician> technicians;
	
	
	public int getServiceId() {
		return serviceId;
	}

	public void setServiceId(int serviceId) {
		this.serviceId = serviceId;
	}

	public String getShortDescription() {
		return shortDescription;
	}

	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	public String getLongDescription() {
		return longDescription;
	}

	public void setLongDescription(String longDescription) {
		this.longDescription = longDescription;
	}

	public double getDuration() {
		return duration;
	}

	public void setDuration(double duration) {
		this.duration = duration;
	}

	public ServiceCategory getCategory() {
		return category;
	}

	public void setCategory(ServiceCategory category) {
		this.category = category;
	}
	
	public List<Technician> getTechnicians() {
		if (technicians == null) {
			technicians = new ArrayList<Technician>();
		}
		return technicians;
	}
	
	// Note lack of setter for collections- preferred use is getList().addAll(otherList)
	
	@Override
	@JsonGetter("deletable")
	@JsonProperty(access = Access.READ_ONLY)
	public boolean isDeletable() {
		return true;
		// TODO: a service can be deleted only if it has no FUTURE appointments associated
	}
	
	@Override
	public int hashCode() {
		return HASH_PRIME * HASH_SEED + serviceId;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Service other = (Service) obj;
		if (serviceId != other.serviceId)
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("Service Entity [");
		sb.append("id = ").append(Integer.toString(serviceId)).append(", ");
		sb.append("Category = ").append(category.getCategoryName()).append(", ");
		sb.append("Description = ").append(shortDescription).append(" ]");
		return sb.toString();
	}
	
}
