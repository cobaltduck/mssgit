package info.cobaltduck.marineservice.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

/**
 * Hibernate entity for a Vessel.
 * A vessel is power boat, sailboat, PWC, or other conveyance intended for
 * on-the-water use.  The shop performs repair and maintenance service on vessels.
 *  
 * @author Wade J. Love
 *
 */
@Entity
@Table(name = "vessel")
public class Vessel implements IMarineServiceEntity {
	
	@Id
	@Column(name = "vessel_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int vesselId;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "customer_id")
	private Customer customer;  //i.e. the owner of the vessel
	
	@Column(name = "vessel_name")
	private String vesselName;
	
	@Column(name = "make")
	private String make;
	
	@Column(name = "model")
	private String model;
	
	@Column(name = "model_year")
	private String modelYear;

	
	public int getVesselId() {
		return vesselId;
	}

	public void setVesselId(int vesselId) {
		this.vesselId = vesselId;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public String getVesselName() {
		return vesselName;
	}

	public void setVesselName(String vesselName) {
		this.vesselName = vesselName;
	}

	public String getMake() {
		return make;
	}

	public void setMake(String make) {
		this.make = make;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getModelYear() {
		return modelYear;
	}

	public void setModelYear(String modelYear) {
		this.modelYear = modelYear;
	}
	
	@Override
	@JsonGetter("deletable")
	@JsonProperty(access = Access.READ_ONLY)
	public boolean isDeletable() {
		return true;
		// TODO: a vessel can be deleted only if it has no associated FUTURE appointments
	}
	
	@Override
	public int hashCode() {
		return HASH_PRIME * HASH_SEED + vesselId;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vessel other = (Vessel) obj;
		if (vesselId != other.vesselId)
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("Vessel Entity [");
		sb.append("id = ").append(Integer.toString(vesselId)).append(", ");
		sb.append("Name = ").append(vesselName).append(", ");
		sb.append("Make = ").append(make).append(", ");
		sb.append("Model = ").append(model).append(", ");
		sb.append("Year = ").append(modelYear).append(" ]");
		return sb.toString();
	}
	
}
