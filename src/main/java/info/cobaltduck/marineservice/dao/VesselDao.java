package info.cobaltduck.marineservice.dao;

import org.springframework.stereotype.Repository;

import info.cobaltduck.marineservice.entity.Vessel;

/**
 * Data Access Object for Vessel entity.
 * 
 * @author Wade J. Love
 *
 */
@Repository("vesselDao")
public class VesselDao extends AbstractDao<Vessel> {

	// constructor that sets class
	public VesselDao() {
		this.clazz = Vessel.class;
	}
	
}
