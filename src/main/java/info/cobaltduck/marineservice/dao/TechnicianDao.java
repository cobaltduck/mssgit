package info.cobaltduck.marineservice.dao;

import org.springframework.stereotype.Repository;

import info.cobaltduck.marineservice.entity.Technician;

/**
 * Data Access Object for Technician entity.
 * 
 * @author Wade J. Love
 *
 */
@Repository("technicianDao")
public class TechnicianDao extends AbstractDao<Technician> {

	// constructor that sets class
	public TechnicianDao() {
		this.clazz = Technician.class;
	}
}
