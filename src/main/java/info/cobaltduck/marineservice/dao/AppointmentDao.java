package info.cobaltduck.marineservice.dao;

import org.springframework.stereotype.Repository;

import info.cobaltduck.marineservice.entity.Appointment;

/**
 * Data Access Object for Appointment entity.
 * 
 * @author Wade J. Love
 *
 */
@Repository("appointmentDao")
public class AppointmentDao extends AbstractDao<Appointment> {

	// constructor that sets class
	public AppointmentDao() {
		this.clazz = Appointment.class;
	}
	
	// TODO: Find by technician and find by customer methods
	// The second will require a join on vessel
}
