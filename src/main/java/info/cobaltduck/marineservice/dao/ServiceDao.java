package info.cobaltduck.marineservice.dao;

import org.springframework.stereotype.Repository;

import info.cobaltduck.marineservice.entity.Service;

/**
 * Data Access Object for Service entity.
 * 
 * @author Wade J. Love
 *
 */
@Repository("serviceDao")
public class ServiceDao extends AbstractDao<Service> {

	// constructor that sets class
	public ServiceDao() {
		this.clazz = Service.class;
	}
	
	// TODO: A find by category method
}
