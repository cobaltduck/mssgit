package info.cobaltduck.marineservice.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;

import info.cobaltduck.marineservice.entity.IMarineServiceEntity;

/**
 * Generic Data Access Object that performs basic CRUD operations for all
 * entities defined for this application.  Other Dao's should extend this
 * and add custom functions as needed.
 * 
 * @author Wade J. Love
 *
 * @param <T> any Hibernate entity class used with this application
 */
public abstract class AbstractDao<T extends IMarineServiceEntity> {
	
	@Autowired
	protected HibernateTemplate template;
	protected Class<T> clazz;
	
	public void save(T entity) {
		template.save(entity);
	}
	
	public void update(T entity) {
		template.merge(entity);
	}
	
	public void delete(T entity) {
		template.delete(entity);
	}
	
	public void deleteById(int id) {
		T entity = template.get(clazz,  id);
		template.delete(entity);
	}
	
	public T findById(int id) {
		return template.get(clazz, id);
	}
	
	public List<T> findList() {
		return template.loadAll(clazz);
	}
	
}
