package info.cobaltduck.marineservice.dao;

import org.springframework.stereotype.Repository;

import info.cobaltduck.marineservice.entity.Customer;

/**
 * Data Access Object for Customer entity.
 * 
 * @author Wade J. Love
 *
 */
@Repository("customerDao")
public class CustomerDao extends AbstractDao<Customer> {

	// constructor that sets class
	public CustomerDao() {
		this.clazz = Customer.class;
	}
}
