package info.cobaltduck.marineservice.dao;

import org.springframework.stereotype.Repository;

import info.cobaltduck.marineservice.entity.ServiceCategory;

/**
 * Data Access Object for ServiceCategory entity.
 * 
 * @author Wade J. Love
 *
 */
@Repository("serviceCategoryDao")
public class ServiceCategoryDao extends AbstractDao<ServiceCategory> {

	// constructor that sets class
	public ServiceCategoryDao() {
		this.clazz = ServiceCategory.class;
	}
	
}
