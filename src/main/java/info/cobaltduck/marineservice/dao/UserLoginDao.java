package info.cobaltduck.marineservice.dao;

import org.springframework.stereotype.Repository;

import info.cobaltduck.marineservice.entity.UserLogin;

/**
 * Data Access Object for UserLogin entity.
 * 
 * @author Wade J. Love
 *
 */
@Repository("userLoginDao")
public class UserLoginDao extends AbstractDao<UserLogin> {

	// constructor that sets class
	public UserLoginDao() {
		this.clazz = UserLogin.class;
	}
}
