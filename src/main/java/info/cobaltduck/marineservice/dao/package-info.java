/**
 * Package contains Data Access Objects for this application
 * Each class is annotated as {@code @Repository}
 * @author Wade J. Love
 */
package info.cobaltduck.marineservice.dao;