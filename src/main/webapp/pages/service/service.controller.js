app.controller('serviceCtrl', ['$scope', '$location', '$routeParams', 'MessageUtil', 'BaseCrud',
    function($scope, $location, $routeParams, MessageUtil, BaseCrud) {
		
		/*
		 * *** Scope Variables used by Pages ***
		 * message
		 * serviceList
		 * tableColumns
		 * showDeleteConfirm
		 * isEdit
		 * serviceItem
		 * categoryOptions
		 * buttonStatus  
		 */
	
		$scope.form = { serviceForm: {} };
		
		$scope.initList = function() {
			$scope.getList(false);
		};
		
		$scope.getList = function(fromDelete) {
			BaseCrud.list('service', 'Service', fromDelete).then(function(listResult) {
				$scope.serviceList = extractCategoryFromJson(listResult.list);
				$scope.message = listResult.message;
			});
			setTableColumns();
		};
		
		$scope.load = function() {
			// first thing, need to load the categoryOptions
			BaseCrud.list('category', '', false).then(function(data) {
				if(data) {
			    	$scope.categoryOptions = data;
			    }
			    else {
			    	$scope.message = MessageUtil.setError('Unable to retrieve list of available Service Categories');
			    	return;
			    }
			});
			
			// then proceed
			var isEdit = $routeParams.state === 'edit' ? true : false;
			var id = $routeParams.serviceId;
			$scope.isEdit = isEdit;
			if (isEdit) {
				BaseCrud.retrieve('service', id).then(function(itemResult){
					$scope.serviceItem = itemResult.item;
					$scope.message = itemResult.message;
					$scope.buttonStatus = itemResult.buttonStatus;
				});
			}
			else {
				// proceed to create new mode
				$scope.serviceItem = {};
				$scope.buttonStatus = {save: true, reset: true};
			}
		}
		
		$scope.save = function() {
			if (!validate()) {
				return; // and do nothing
			}
			
			BaseCrud.addOrUpdate('service', 'Service', $scope.serviceItem, $scope.isEdit)
				.then(function(result){
					$scope.message = result.message;
					$scope.buttonStatus = result.buttonStatus;
			});
		};
		
		$scope.deleteConfirm = function(object) {
			$scope.idToDelete = object.serviceId; // passed to deleteItem() if confirmed
			$scope.showDeleteConfirm = true;
		};
		
		$scope.cancelDelete = function() {
			$scope.showDeleteConfirm = false;
		}
		
		$scope.deleteItem = function() {
			$scope.showDeleteConfirm = false;
			BaseCrud.deleteItem('service', $scope.idToDelete).then(function(result) {
				$scope.message = result.message;
			});
			$scope.getList(true); // in order to refresh the list
		};
		
		// Navigation Functions
		$scope.newItem = function() {
			$location.path('serviceform/new/new');
		};
		
		$scope.editItem = function(object) {
			$location.path('serviceform/edit/'+object.serviceId);
		};
		
		$scope.reset = function() {
			$scope.message = {};
			$scope.form.serviceForm.$setUntouched();
			$scope.load(); // re-places values of edited item, blanks out a new item
		};
		
		$scope.back = function() {
			$location.path('servicelist');
		};
		
		// Private internal functions
		function extractCategoryFromJson(serviceData) {
			// This function "promotes" category name to a first-level attribute, from an attribute of a child element
			serviceData.forEach(function(element) {
				element.catName = element.category.categoryName;
			});
			return serviceData;
		}
		
		function setTableColumns() {
			$scope.tableColumns = [
			    {
			    	headerText: 'Id',
			    	attribute: 'serviceId',
			    	align: 'aln_center'
			    },
			    {
			    	headerText: 'Category',
			    	attribute: 'catName'
			    },
			    {
			    	headerText: 'Description (hover for more)',
			    	attribute: 'shortDescription',
			    	tooltip: 'longDescription'
			    },
			    {
			    	headerText: 'Hours',
			    	attribute: 'duration',
			    	align: 'aln_right'
			    }
			];
		}
		
		function validate() {
			// Required: category, short, long, duration
			if($scope.serviceItem.category != null && 
					$scope.serviceItem.shortDescription != null &&
					$scope.serviceItem.shortDescription != '' &&
					$scope.serviceItem.longDescription != null &&
					$scope.serviceItem.longDescription != '' &&
					$scope.serviceItem.duration != null &&
					$scope.serviceItem.duration >= 0.1	
			) {
				return true;
			}
			else {
				angular.forEach($scope.form.serviceForm.$error, function (field) {
			        angular.forEach(field, function(theField){
			            theField.$setTouched();
			        })
			    });
				$scope.message = MessageUtil.setValidationError();
				return false;
			}
		};
		
	}
]);