app.controller('categoryCtrl', ['$scope', '$location', '$routeParams', 'MessageUtil', 'BaseCrud',
    function($scope, $location, $routeParams, MessageUtil, BaseCrud) {
		
		/*
		 * *** Scope Variables used by Pages ***
		 * message
		 * categoryList
		 * tableColumns
		 * showDeleteConfirm
		 * isEdit
		 * categoryItem
		 * buttonStatus  
		 */
		
		$scope.form = { categoryForm: {} };
	
		$scope.initList = function() {
			$scope.getList(false);
		};
		
		$scope.getList = function(fromDelete) {
			BaseCrud.list('category', 'Service Category', fromDelete).then(function(listResult) {
				$scope.categoryList = listResult.list;
				$scope.message = listResult.message;
			});
			setTableColumns();
		};
		
		$scope.load = function() {
			var isEdit = $routeParams.state === 'edit' ? true : false;
			var id = $routeParams.categoryId;
			$scope.isEdit = isEdit;
			if (isEdit) {
				BaseCrud.retrieve('category', id).then(function(itemResult){
					$scope.categoryItem = itemResult.item;
					$scope.message = itemResult.message;
					$scope.buttonStatus = itemResult.buttonStatus;
				});
			}
			else {
				// proceed to create new mode
				$scope.categoryItem = {};
				$scope.buttonStatus = {save: true, reset: true};
			}
		}
		
		$scope.save = function() {
			if (!validate()) {
				return; // and do nothing
			}
			
			BaseCrud.addOrUpdate('category', 'Service Category', $scope.categoryItem, $scope.isEdit)
				.then(function(result){
					$scope.message = result.message;
					$scope.buttonStatus = result.buttonStatus;
			});
		};
		
		$scope.deleteConfirm = function(object) {
			$scope.idToDelete = object.categoryId; // passed to deleteItem() if confirmed
			$scope.showDeleteConfirm = true;
		};
		
		$scope.cancelDelete = function() {
			$scope.showDeleteConfirm = false;
		}
		
		$scope.deleteItem = function() {
			$scope.showDeleteConfirm = false;
			BaseCrud.deleteItem('category', $scope.idToDelete).then(function(result) {
				$scope.message = result.message;
			});
			$scope.getList(true); // in order to refresh the list
		};
		
		// Navigation Functions
		$scope.newItem = function() {
			$location.path('categoryform/new/new');
		};
		
		$scope.editItem = function(object) {
			$location.path('categoryform/edit/'+object.categoryId);
		};
		
		$scope.reset = function() {
			$scope.message = {};
			$scope.form.categoryForm.$setUntouched();
			$scope.load(); // re-places values of edited item, blanks out a new item
		};
		
		$scope.back = function() {
			$location.path('categorylist');
		};
		
		// Private internal functions
		function setTableColumns() {
			$scope.tableColumns = [
			    {
			    	headerText: 'Id',
			    	attribute: 'categoryId',
			    	align: 'aln_center'
			    },
			    {
			    	headerText: 'Name',
			    	attribute: 'categoryName'
			    }
			];
		}
		
		function validate() {
			// This form just needs a name
			if($scope.categoryItem.categoryName != null && 
					$scope.categoryItem.categoryName != '') {
				return true;
			}
			else {
				angular.forEach($scope.form.categoryForm.$error, function (field) {
			        angular.forEach(field, function(theField){
			            theField.$setTouched();
			        })
			    });
				$scope.message = MessageUtil.setValidationError();
				return false;
			}
		}
		
	}
]);