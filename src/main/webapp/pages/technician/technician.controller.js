app.controller('technicianCtrl', ['$scope', '$location', '$routeParams', 'MessageUtil', 'BaseCrud',
    function($scope, $location, $routeParams, MessageUtil, BaseCrud) {
		
		var MIN_BREAKTIME = parseTimeStringToDate("10:00:00");
		var MAX_BREAKTIME = parseTimeStringToDate("14:30:00");
		
		/*
		 * *** Scope Variables used by Pages ***
		 * message
		 * technicianList
		 * tableColumns
		 * showDeleteConfirm
		 * isEdit
		 * technicianItem
		 * buttonStatus  
		 */
		
		$scope.form = { technicianForm: {} };
	
		$scope.initList = function() {
			$scope.getList(false);
		};
		
		$scope.getList = function(fromDelete) {
			BaseCrud.list('technician', 'Technician', fromDelete).then(function(listResult) {
				$scope.technicianList = listResult.list;
				$scope.message = listResult.message;
			});
			setTableColumns();
		};
		
		$scope.load = function() {
			var isEdit = $routeParams.state === 'edit' ? true : false;
			var id = $routeParams.technicianId;
			$scope.isEdit = isEdit;
			if (isEdit) {
				BaseCrud.retrieve('technician', id).then(function(itemResult){
					itemResult.item.breakhour = parseTimeStringToDate(itemResult.item.breakhour);
					$scope.technicianItem = itemResult.item;
					$scope.message = itemResult.message;
					$scope.buttonStatus = itemResult.buttonStatus;
				});
			}
			else {
				// proceed to create new mode
				// In this case, with a couple defaults
				$scope.technicianItem = {
					"breakhour":parseTimeStringToDate("12:00:00"),
					"user":{ "userType" : "TECHNICIAN" }
				};
				$scope.buttonStatus = {save: true, reset: true};
			}
		}
		
		$scope.save = function() {
			if (!validate()) {
				return; // and do nothing
			}
			BaseCrud.addOrUpdate('technician', 'Technician', $scope.technicianItem, $scope.isEdit)
				.then(function(result){
					$scope.message = result.message;
					$scope.buttonStatus = result.buttonStatus;
			});
		};
		
		$scope.deleteConfirm = function(object) {
			$scope.idToDelete = object.technicianId; // passed to deleteItem() if confirmed
			$scope.showDeleteConfirm = true;
		};
		
		$scope.cancelDelete = function() {
			$scope.showDeleteConfirm = false;
		}
		
		$scope.deleteItem = function() {
			$scope.showDeleteConfirm = false;
			BaseCrud.deleteItem('technician', $scope.idToDelete).then(function(result) {
				$scope.message = result.message;
			});
			$scope.getList(true); // in order to refresh the list
		};
		
		// Navigation Functions
		$scope.newItem = function() {
			$location.path('technicianform/new/new');
		};
		
		$scope.editItem = function(object) {
			$location.path('technicianform/edit/'+object.technicianId);
		};
		
		$scope.reset = function() {
			$scope.message = {};
			$scope.form.technicianForm.$setUntouched();
			$scope.load(); // re-places values of edited item, blanks out a new item
		};
		
		$scope.back = function() {
			$location.path('technicianlist');
		};
		
		// Private internal functions
		function setTableColumns() {
			$scope.tableColumns = [
			    {
			    	headerText: 'Id',
			    	attribute: 'technicianId',
			    	align: 'aln_center'
			    },
			    {
			    	headerText: 'Last Name',
			    	attribute: 'lastname'
			    },
			    {
			    	headerText: 'First Name',
			    	attribute: 'firstname'
			    },
			    {
			    	headerText: 'Break',
			    	attribute: 'breakhour'
			    }
			];
		}
		
		function validate() {
			// Required: first, last, breakhour, breakhour in range, and if new, user and password
			if($scope.technicianItem.firstname != null && 
					$scope.technicianItem.firstname != '' &&
					$scope.technicianItem.lastname != null &&
					$scope.technicianItem.lastname != '' &&
					$scope.technicianItem.breakhour != null &&
					$scope.technicianItem.breakhour	>= MIN_BREAKTIME &&
					$scope.technicianItem.breakhour <= MAX_BREAKTIME &&
					($scope.isEdit || (
							$scope.technicianItem.user.username != null &&
							$scope.technicianItem.user.username != '' &&
							$scope.technicianItem.user.password != null &&
							$scope.technicianItem.user.password != ''
					))
					
			) {
				return true;
			}
			else {
				angular.forEach($scope.form.technicianForm.$error, function (field) {
			        angular.forEach(field, function(theField){
			            theField.$setTouched();
			        })
			    });
				$scope.message = MessageUtil.setValidationError();
				return false;
			}
		}
		
		function parseTimeStringToDate(timeString) {
			// takes in a time like "12:30:00" and returns as JS date object bound to epoch
			// Needed for AngularJS input type="time"; see https://docs.angularjs.org/api/ng/input/input%5Btime%5D
			// if timeString enters in an invalid format, this will create an exception
			// Note that even like this, AngularJS still throws a warning, but at least not an error.
			var parts = timeString.split(":");   // parts array holds HH, mm, ss
			var dateResult = new Date(1970,0,1,parts[0],parts[1],parts[2]);
			return dateResult;
		}
		
	}
]);