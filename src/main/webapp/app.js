var app = angular.module('marineService', ['ngRoute']);

// route config
app.config(['$locationProvider', '$routeProvider',
    function ($locationProvider, $routeProvider) {
		$locationProvider.hashPrefix('!');
		
		$routeProvider.
			when('/home', {
				templateUrl: 'pages/home.html',
				controller: 'homeCtrl'
			}).
			when('/categorylist', {
				templateUrl: 'pages/category/categorylist.html',
				controller: 'categoryCtrl'
			}).
			when('/categoryform/:state/:categoryId', {
				templateUrl: 'pages/category/categoryform.html',
				controller: 'categoryCtrl'
			}).
			when('/servicelist', {
				templateUrl: 'pages/service/servicelist.html',
				controller: 'serviceCtrl'
			}).
			when('/serviceform/:state/:serviceId', {
				templateUrl: 'pages/service/serviceform.html',
				controller: 'serviceCtrl'
			}).
			when('/technicianlist', {
				templateUrl: 'pages/technician/technicianlist.html',
				controller: 'technicianCtrl'
			}).
			when('/technicianform/:state/:technicianId', {
				templateUrl: 'pages/technician/technicianform.html',
				controller: 'technicianCtrl'
			}).
			otherwise('/home')
		;
	}
]);
