app.directive('messageBar', function(){
	return {
		restrict: 'C',
		templateUrl: 'components/messageBar/messageBar.html',
		scope: {
			messageObject: '='
		}
	};
});