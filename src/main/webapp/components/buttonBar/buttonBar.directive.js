app.directive('buttonBar', function(){
	return {
		restrict: 'C',
		templateUrl: 'components/buttonBar/buttonBar.html',
		scope: {
			buttonStatus: '=',
			savefn: '&',
			resetfn: '&',
			addanotherfn: '&',
			backfn: '&'
		}
	};
});