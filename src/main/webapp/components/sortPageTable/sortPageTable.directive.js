app.directive('sortPageTable', ['$timeout', function($timeout){
	return {
		restrict: 'C',
		templateUrl: 'components/sortPageTable/sortPageTable.html',
		scope: {
			rows: '=',
			columns: '=',
			editable: '=',
			newable: '=',
			deletable: '=',
			editfn: '&',
			deletefn: '&',
			newfn: '&'
		},
		controllerAs: 'sortPageTableCtrl',
		controller: ['$scope', function($scope) {
			
			$scope.currentPage = 0;
			$scope.pageSize = 10;
			
			// initialize sort order (need to wait until scope is applied)
			// Right now, this only works on FIRST page load, fails if leave and return)
			// commenting out as a non-essential function, may return to later.
			/*$timeout(function() {
				$scope.sortKey = $scope.columns[0].attribute;
				$scope.reverse = false;
			});*/
			
			$scope.sort = function(sortKey) {
				$scope.sortKey = sortKey;
				$scope.reverse = !$scope.reverse;
			};
			
			$scope.numberOfPages = function() {
				if ($scope.rows ) {  // this function is called twice on load, but vars are undefined the first time- this prevents undef. errors
					return Math.ceil($scope.rows.length / $scope.pageSize);
				}
			};
			
		}]
	};
}]);

// credit: http://jsfiddle.net/2ZzZB/56/
app.filter('startFrom', function() {
	return function(input, start) {
		start = parseInt(start);
		if (input) { // this function is called twice on load, but vars are undefined the first time- this prevents undef. errors
			return input.slice(start);
		}
	};
});