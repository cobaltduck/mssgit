app.directive('modalDialog', function(){
	return {
		restrict: 'C',
		templateUrl: 'components/modalDialog/modalDialog.html',
		scope: {
			dialogText: '=',
			confirmfn: '&',
			cancelfn: '&',
		}
	};
});