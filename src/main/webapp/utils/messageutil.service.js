/* Utility methods for messageBar
 * Controller optionally sets the message text, this util returns
 * an object with that text or default text, and a proper CSS class.
 */
app.factory('MessageUtil', [ function() {
	
	var me = { MessageUtil: [] };
	
	me.setInfo = function(messageText) {
		if (!messageText) {
			messageText = 'Your request is in progress.';
		}
		var message = {
			available: true,
			text: messageText,
			colorClass: 'w3-blue'
		}
		return message;
	};
	
	me.setSuccess = function(messageText) {
		if (!messageText) {
			messageText = 'The operation completed successfully.';
		}
		var message = {
			available: true,
			text: messageText,
			colorClass: 'w3-green'
		}
		return message;
	};
	
	me.setError = function(messageText) {
		if (!messageText) {
			messageText = 'The operation failed.  Please try again.  If the error persists, please contact system administrator';
		}
		var message = {
			available: true,
			text: messageText,
			colorClass: 'w3-red'
		}
		return message;
	};
	
	me.setValidationError = function(messageText) {
		if (!messageText) {
			messageText = 'The form could not be submitted due to validation errors.  Please check the form and try again.';
		}
		var message = {
			available: true,
			text: messageText,
			colorClass: 'w3-orange'
		}
		return message;
	};
	
	return me;
	
}]);