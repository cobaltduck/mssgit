/*
 * Separates out boiler-plate calls to the Spring MVC layer, allowing
 * controllers to remain somewhat D.R.Y.
 * Errors and caught and logged here, and recorded as message objects.
 * Each controller still needs to translate these results into its own scope,
 * so there is still some repetitive code in the controllers.
 */
app.factory('BaseCrud', ['$http', 'MessageUtil', function($http, MessageUtil) {
	
	var me = { BaseCrud: [] };
	var urlBase = '/MarineServiceScheduler/websvc/';
	
	me.list = function(urlFragment, entityName, fromDelete) {
		var urlExtension = urlFragment + '/list';
		return doGet(urlExtension).then(function(data) {
			var result = {};
		    if(data) {
		    	result.list = data;
		    	result.message = 
		    		fromDelete? MessageUtil.setSuccess(entityName + ' was deleted successfully')
		    				: {};
		    }
		    else {  // data is null or false
		    	result.message = 
		    		fromDelete? MessageUtil.setError(entityName + ' was deleted successfully, but unable to refresh list')
		    				: MessageUtil.setError();
		    }
		    return result;
		});
	};
	
	me.retrieve = function(urlFragment, itemId) {
		var urlExtension = urlFragment + '/retrieve/' + itemId
		return doGet(urlExtension).then(function(data) {
			var result = {};
			if (data) {
				result.item = data;
				result.message = {};
				result.buttonStatus = {save: true, reset:true};
			}
			else {
				result.item = {};
				result.message = MessageUtil.setError();
				result.buttonStatus = {save: false, reset:true};
			}
			return result;
		});
	};
	
	me.addOrUpdate = function(urlFragment, entityName, jsonPayload, isEdit) {
		var param = JSON.stringify(jsonPayload);
		var url = isEdit ? 'update' : 'add';
		url = urlFragment + '/' + url;
		return doPost(url, param).then(function(success) {
			var result = {};
			if (success) {
				if(isEdit) {
					result.message = MessageUtil.setSuccess(entityName + ' was updated successfully');
					result.buttonStatus = {};
				}
				else {
					result.message = MessageUtil.setSuccess(entityName + ' was created successfully');
					result.buttonStatus = {addAnother: true};
				}
			}
			else {
				result.message = MessageUtil.setError();
			}
			return result;
		});
	};
	
	me.deleteItem = function(urlFragment, itemId) {
		var urlExtension = urlFragment + '/delete/' + itemId
		return doGet(urlExtension).then(function(success) {
			var result = {};
			if (success) {
				result.message = null;
			}
			else {  // null or false
				result.message = MessageUtil.setError();
			}
			return result;
		});
	};
	
	
	
	// (private) internal functions
	function doPost(urlExtension, param) {
		var url = urlBase + urlExtension;
		return $http.post(url, param).then(
			function(response) {
				if (response && response.data === true) {
					return true;
				}
				else {
					console.log("Null or false response from doPost at " + url);
					return false;
				}
			},
			function(error) {
			   	console.log(error);
			   	return false;
			}
		);
	};
	
	function doGet(urlExtension) {
		var url = urlBase + urlExtension;
		return $http.get(url).then(
			function(response) {
				if (response && response.data) {
					return response.data;
					// Note that on delete, response.data should be the single result "true"
				}
				else {
					console.log("Null or false response from doGet at " + url);
					return false;
				}
			},
			function(error) {
				console.log(error);
				return false;
			}
		);
	};
	
	
	return me;
	
}]);