/* copy paste to call this script:
	source C:/Users/Wade J. Love/Documents/workspace/MarineServiceScheduler/sql/reset.sql;
*/

/* reset the database itself */
DROP DATABASE IF EXISTS marineservice;
CREATE DATABASE marineservice;

/* call the setup scripts in turn */
source C:/Users/Wade J. Love/Documents/workspace/MarineServiceScheduler/sql/ddl.sql;
source C:/Users/Wade J. Love/Documents/workspace/MarineServiceScheduler/sql/test_data_init.sql;
/* Add other setup sql files here if neeeded */
