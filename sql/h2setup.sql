/* copy paste to call this script:
	RUNSCRIPT from 'C:/Users/Wade J. Love/Documents/workspace/MarineServiceScheduler/sql/h2setup.sql'
*/

/* clear old versions of tables */
DROP TABLE IF EXISTS customer CASCADE;
DROP TABLE IF EXISTS user_login CASCADE;
DROP TABLE IF EXISTS vessel CASCADE;
DROP TABLE IF EXISTS appointment CASCADE;
DROP TABLE IF EXISTS service CASCADE;
DROP TABLE IF EXISTS technician CASCADE;
DROP TABLE IF EXISTS service_category CASCADE;
DROP TABLE IF EXISTS skill CASCADE;

/* create tables */
CREATE TABLE user_login (
	user_id			INT(8)			NOT NULL AUTO_INCREMENT,
	username		VARCHAR(32)		NOT NULL,
	password		VARCHAR(64)		NOT NULL, /*encrypted*/
	user_type		VARCHAR(16)		NOT NULL,
	CONSTRAINT uerloginpk PRIMARY KEY (user_id)
);

CREATE TABLE service_category (
	category_id		INT(8)			NOT NULL AUTO_INCREMENT,
	category_name	VARCHAR(32)		NOT NULL,
	CONSTRAINT servicecategorypk PRIMARY KEY (category_id)
);

CREATE TABLE service (
	service_id		INT(8)			NOT NULL AUTO_INCREMENT,
	short_desc		VARCHAR(100)	NOT NULL,
	long_desc		TEXT,
	duration		FLOAT			NOT NULL,
	category		INT(8)			NOT NULL,
	CONSTRAINT servicepk PRIMARY KEY (service_id),
	CONSTRAINT servicecategoryfk FOREIGN KEY (category) REFERENCES service_category(category_id)
);

CREATE TABLE technician (
	technician_id	INT(8)			NOT NULL AUTO_INCREMENT,
	user_id			INT(8)			NOT NULL,
	firstname		VARCHAR(32)		NOT NULL,
	lastname		VARCHAR(32)		NOT NULL,
	breakhour		TIME,
	CONSTRAINT technicianpk PRIMARY KEY (technician_id),
	CONSTRAINT technicianuseridfk FOREIGN KEY (user_id) REFERENCES user_login(user_id)
);

CREATE TABLE skill (
	technician_id	INT(8)			NOT NULL,
	service_id		INT(8)			NOT NULL,
	CONSTRAINT skillpk PRIMARY KEY (technician_id, service_id),
	CONSTRAINT skilltechnicianfk FOREIGN KEY (technician_id) REFERENCES technician(technician_id),
	CONSTRAINT skillservicefk FOREIGN KEY (service_id) REFERENCES service(service_id)
);

CREATE TABLE customer (
	customer_id		INT(8)			NOT NULL AUTO_INCREMENT,
	user_id			INT(8),
	firstname		VARCHAR(32)		NOT NULL,
	lastname		VARCHAR(32)		NOT NULL,
	addr_line1		VARCHAR(100)	NOT NULL,
	addr_line2		VARCHAR(100),
	addr_city		VARCHAR(32)		NOT NULL,
	addr_state		VARCHAR(2)		NOT NULL,
	addr_zip		VARCHAR(9)		NOT NULL,
	phone			VARCHAR(10),
	email			VARCHAR(100),
	prefer_email	BOOLEAN,
	CONSTRAINT customerpk PRIMARY KEY (customer_id),
	CONSTRAINT customeruserfk FOREIGN KEY (user_id) REFERENCES user_login(user_id)
);

CREATE TABLE vessel (
	vessel_id		INT(8)			NOT NULL AUTO_INCREMENT,
	customer_id		INT(8)			NOT NULL,
	vessel_name		VARCHAR(32),
	make			VARCHAR(32)		NOT NULL,
	model			VARCHAR(32),
	model_year		VARCHAR(5),
	CONSTRAINT vesselpk PRIMARY KEY (vessel_id),
	CONSTRAINT vesselcustomerfk FOREIGN KEY (customer_id) REFERENCES customer(customer_id)
);	

CREATE TABLE appointment (
	appointment_id	INT(8)			NOT NULL AUTO_INCREMENT,
	vessel_id		INT(8)			NOT NULL,
	service_id		INT(8)			NOT NULL,
	technician_id	INT(8)			NOT NULL,
	start_time		DATETIME,
	end_time		DATETIME,
	CONSTRAINT appointmentpk PRIMARY KEY (appointment_id),
	CONSTRAINT appointmentvesselfk FOREIGN KEY (vessel_id) REFERENCES vessel(vessel_id),
	CONSTRAINT appointmentservicefk FOREIGN KEY (service_id) REFERENCES service(service_id),
	CONSTRAINT appointmenttechnicianfk FOREIGN KEY (technician_id) REFERENCES technician(technician_id)
);