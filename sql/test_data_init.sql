/* copy paste to call this script:
	source C:/Users/Wade J. Love/Documents/workspace/MarineServiceScheduler/sql/test_data_init.sql;
 */
 
 USE marineservice;
 
 /* start with a clean slate */
DELETE FROM skill;
DELETE FROM appointment;
DELETE FROM vessel;
DELETE FROM service;
DELETE FROM technician;
DELETE FROM customer;
DELETE FROM user_login;
DELETE FROM service_category;


INSERT INTO service_category (category_name)
	VALUES ('Sailboat Rigging');
SET @cat1 = LAST_INSERT_ID();

INSERT INTO service_category (category_name)
	VALUES ('Sail sewing work');
SET @cat2 = LAST_INSERT_ID();

INSERT INTO service_category (category_name)
	VALUES ('Engine Service - Inboard or I/O');
SET @cat3 = LAST_INSERT_ID();

INSERT INTO service_category (category_name)
	VALUES ('Engine Service - Outboard');
SET @cat4 = LAST_INSERT_ID();

INSERT INTO service_category (category_name)
	VALUES ('Metal Hull Repair/ Mntnce');
SET @cat5 = LAST_INSERT_ID();

INSERT INTO service_category (category_name)
	VALUES ('Fiberglass Hull Repair/ Mntnce');
SET @cat6 = LAST_INSERT_ID();

INSERT INTO service_category (category_name)
	VALUES ('Fitting Installation or Repair');
SET @cat7 = LAST_INSERT_ID();

INSERT INTO service_category (category_name)
	VALUES ('Painting');
SET @cat8 = LAST_INSERT_ID();
	
	
INSERT INTO service (short_desc, long_desc, duration, category)
	VALUES('Replace metal standing rigging',
		'Replace stays, shrouds, and lifts made from metal cable or wire, including attachment points and turnbuckles where required', 2.0, @cat1);	
SET @svc1 = LAST_INSERT_ID();

INSERT INTO service (short_desc, long_desc, duration, category)
	VALUES('Install block',
		'Install a block (pulley) for halyards, sheets, or boom vangs', 1.5, @cat1);	
SET @svc2 = LAST_INSERT_ID();

INSERT INTO user_login (username, password, user_type)
	VALUES ('rocknrandy', SHA2('!QAZ2wsx',256), 'TECHNICIAN');
SET @usr1 = LAST_INSERT_ID();

INSERT INTO technician (user_id, firstname, lastname, breakhour)
	VALUES (@usr1, 'Randy', 'Edwards', '11:30');
SET @tech1 = LAST_INSERT_ID();

INSERT INTO skill (technician_id, service_id) VALUES (@tech1, @svc1);
INSERT INTO skill (technician_id, service_id) VALUES (@tech1, @svc2);
